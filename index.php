<?php

session_start();

include 'core/connexion_bdd.php';
include 'core/rand_word.php';
include 'core/keyboard.php';

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Jeu du pendu</title>
  <!-- Personalized Bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <link rel="stylesheet" href="templates/style.css">
</head>
<body class="text-center">
  <div class="container">
    <div class="row">
      <h1>Le jeu du pendu</h1>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-3 col-md-offset-3">
        <?php
        $_SESSION['fails'] = array();
        $success=false;

        if (empty($_SESSION['essais'])) {
          $_SESSION['essais']=8;
        }

        if (empty($_SESSION['word'])) {
          $_SESSION['word']=rand_word();
          $_SESSION['word'] = strtoupper($_SESSION['word']);
          $_SESSION['guess_word'] = $_SESSION['word'];
          for ($i=0; $i < strlen($_SESSION['word']); $i++) {
            $_SESSION['guess_word'][$i]=str_replace($_SESSION['word'][$i],"_",$_SESSION['word'][$i]);
          }
        }

        if (empty($_GET['letter'])) {
          echo "<p>Pour jouer, sélectionnez une lettre</p>";
          $lettre=NULL;
        } else {
          $lettre=$_GET['letter'];
        }

        for ($i=0; $i < strlen($_SESSION['word']); $i++) {
          if ($_SESSION['word'][$i]==$lettre) {
            $_SESSION['guess_word'][$i]=$lettre;
            $success=true;
          }
        }

        echo "<h2>";
        for ($i=0; $i < strlen($_SESSION['word']); $i++) {
          echo $_SESSION['guess_word'][$i] . " ";
        }
        echo "</h2>";

        if ($success) {
          echo '<p>Essais restants ' . $_SESSION['essais'] . '</p>';
        } else {
          $_SESSION['essais']=$_SESSION['essais']-1;
          echo '<p>Essais restants ' . $_SESSION['essais'] . '</p>';
          $_SESSION['fails'][] = $lettre;
        }
        ?>
      </div>
      <div class="col-sm-12 col-md-3 img-responsive">
        <?php
        echo "<img src='images/pendu" . $_SESSION['essais'] . ".png' alt='pendu" . $_SESSION['essais'] . ".png'>";
        ?>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-6 col-md-offset-3">
        <?php
        if ($_SESSION['essais']==0) {
          echo "<p class='end'>Vous avez été pendu !</p>";
        } elseif ($_SESSION['guess_word']==$_SESSION['word']) {
          echo "<p class='end'>Vous avez échappé à votre mort !</p>";
        } else {
          echo '<div>' . keyboard() . '</div>';
        }

        //for ($i=0; $i < count($_SESSION['fails']); $i++) {
        //  echo '<br>' . $_SESSION['fails'][$i];
        //}
         ?>
        <a class="restart btn btn-danger btn-lg" href='core/restart.php'>Recommencer</a>
      </div>
    </div>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
